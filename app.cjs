const toggle = document.getElementById('choice')

toggle.addEventListener('input', (e) => {
    let basicPrice, professionalPrice, masterPrice
    if (toggle.value === '0') {
        basicPrice = '$199.99'
        professionalPrice = '$249.99'
        masterPrice = '$399.99'
    } else {
        basicPrice = '$19.99'
        professionalPrice = '$24.99'
        masterPrice = '$39.99'
    }
    document.getElementById('basic').textContent = basicPrice
    document.getElementById('professional').textContent = professionalPrice
    document.getElementById('master').textContent = masterPrice
})






